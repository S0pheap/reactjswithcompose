import './App.css';
import { useEffect, useState } from 'react';
import api from "./api/api";
import Nav from 'react-bootstrap/Nav';
import Table from 'react-bootstrap/Table';

function App() {

  const [user, setUser] = useState([]);

  useEffect(() => {
    api.get("/v1/students").then((res) => {
      setUser(res.data);
    });
  }, []);
  return (
    <div className="container">
      <Nav
      activeKey="/home" 
      onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
    >
      <Nav.Item>
        <Nav.Link href="/home">Active</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-1">Link</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-2">Link</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="disabled" disabled>
          Disabled
        </Nav.Link>
      </Nav.Item>
    </Nav>
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Address</th>
        </tr>
      </thead>
      <tbody>
        {user.map((item) => {
          return(
          <tr>
            <td></td>
                      <td>{item.name}</td>
                      <td>{item.address}</td>
                    </tr>
          )
        })}
      </tbody>
    </Table>
    <h1>Hello world</h1>
    <h1>Hello devops</h1>
    </div>
  );
}

export default App;
