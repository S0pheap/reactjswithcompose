import axios from 'axios';

const api = axios.create({
    baseURL: 'http://backend:8080/api'
});
export default api
